module.exports = {
    presets: [
        "@babel/preset-env","@vue/app"
    ],
    "sourceMaps": "inline",
    "retainLines": true,
    plugins: [
        "@babel/plugin-proposal-optional-chaining",
        "@babel/plugin-proposal-class-properties"
    ]
};