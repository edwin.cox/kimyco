import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import VueI18n from 'vue-i18n'
import router from './router'
import axios from 'axios'

import { library } from "@fortawesome/fontawesome-svg-core";
import { faHome, faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { languages, defaultLocale } from './locales/index.js'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { FungusController } from "./controllers/FungusController.js";


Vue.use(BootstrapVue)

Vue.config.productionTip = false

library.add(faHome,faAngleLeft,faAngleRight);

Vue.component("font-awesome-icon", FontAwesomeIcon);

// Internationalization
Vue.use(VueI18n)
const messages = Object.assign(languages)
var i18n = new VueI18n({
  locale: defaultLocale,
  fallbackLocale: 'fr',
  messages
})

Vue.prototype.$spring = axios.create({
  baseURL: "http://localhost:3000",
  timeout: 1000,
  headers: {
    'X-Custom-Header': 'foobar',
    'Access-Control-Allow-Origin': '*'
  }
});


new Vue({
  router,
  i18n,
  data () {
    return {
      currentFungus: new FungusController()
    }
  },
  render: h => h(App),
}).$mount('#app')
