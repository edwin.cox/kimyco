export class FungusController {
    
    constructor() {
        console.log(222);
      }

    data = null;

    getFungusById = async (id) => {
        const 
            response = await fetch('http://localhost:3000/Fungus/'+id),
            fungus = await response.json();
        this.data=fungus;
        return this.data;
    }

    getNomenclature = () => {
        if(this.data){
            return [ ...this.data.nomenclature.division, ...this.data.nomenclature.classe, ...this.data.nomenclature.order, ...this.data.nomenclature.family, ...this.data.nomenclature.genus];
        }else{
            return []
        }
        };
    getAttributes = () => {
        return this.data?.parts;
    };
    getAliases = () => {
        return this.data?.names.latin.join(", ");
    };
    getCommonNames = (locale) => {
        return this.data?.names.locales[locale].join(", ");
    };
}

