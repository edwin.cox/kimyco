import fr from './fr.json'

export const defaultLocale = 'fr'
// Create VueI18n instance with options
export const languages = {
  fr: fr
}