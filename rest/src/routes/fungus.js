import { Router } from 'express';

const router = Router();

/**
 * @swagger
 * /fungus/:
 *    get:
 *      description: This should return all fungus.
 *      responses:
 *        200:
 *          description: OK
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Fungus'
 */
router.get('/', (req, res) => {
  return res.send(Object.values(req.context.models.fungus));
});

/**
 * @swagger
 * /fungus/{fungusId}:
 *   get:
 *      description: This should return one fungus.
 *      parameters:
 *        - name: fungusId
 *          in: path
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: OK
 *          schema:
 *            description: fungus
 *            $ref: '#/definitions/Fungus'
 */
router.get('/:fungusId', (req, res) => {
  return res.send(req.context.models.fungus[req.params.fungusId]);
});

export default router;
