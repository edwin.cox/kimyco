/**
 * @swagger
 * definitions:
 *      Fungus:
 *          type: object
 *          properties:
 *              id:
 *                  type: integer
 *                  format: int64
 *              names:
 *                  type: object
 *                  properties:
 *                      latin:
 *                          type: array
 *                          items:
 *                              type: string
 *                      locales:
 *                          type: object
 *                          additionalProperties:
 *                              type: array
 *                              items:
 *                                  type: string
 *                          example:
 *                              fr: ['Tricholome blanc']
 *                              en: ['white knight']
 *              nomenclature:
 *                  type: object
 *                  properties:
 *                      division:
 *                          type: array
 *                          items:
 *                              type: string
 *                      classe:
 *                          type: array
 *                          items:
 *                              type: string
 *                      order:
 *                          type: array
 *                          items:
 *                              type: string
 *                      family:
 *                          type: array
 *                          items:
 *                              type: string
 *                      genus:
 *                          type: array
 *                          items:
 *                              type: string
 *              description:
 *                  type: string
 *              comestibility:
 *                  type: string
 *              parts:
 *                  type: object
 *                  additionalProperties:
 *                      type: object
 *                      additionalProperties:
 *                          type: array
 *                          items:
 *                              oneOf: 
 *                              - type: string
 *                              - type: number
 */


const fungus =  {
    1:{
        id: 1,
        names: {
            latin: ["Tricholoma album"],
            locales: {
                "fr": ["Tricholome blanc","test1"]
            }
        },
        nomenclature: {
            division: ["Basidiomycete"],
            classe: ["Homobasidiomycetes","Agaricomycetideae"],
            order: ["Tricholomatales"],
            family: ["Tricholomataceae"],
            genus: ["Tricholoma"]
        },
        description: "Saveur âcre ou poivrée; lames +/- serrées.",
        comestibility: "Non comestible",
        parts: {
            cap: {
                radius:[1,1.5],
                height:[0.3,0.7],
                colour: [ "#FFFFFF" ],
                viscosity: ["sec"]
            },
            foot: {
                consistancy: ["creux"],
                length: [3,5],
                radius: [0.3,0.4],
                colour: [ "#FFFFFF", "#AAAAAA" ],
                centered: true,
                veil: []
            },
            lamelle: {
                colour: [ "#ddccc0ec" ],
                shape: ["echancré"],
                gap: ["serré"],
                forked: false,
                merged: false
            },
            spore: {
                colour: [ "#ddccc0ec" ],
                size: [0.001]
            },
            flesh: {
                consistancy:["fibreux","souple"],
                milk: false,
                colourChange: ["immuable"]
            },
            general: {
                taste: ["âcre","poivré"],
                odor: ["nul"],
                habitat:["forêt","conifère","prairie"],
                hymenium: "extern"
            } 
        }
    }
};

export default fungus;