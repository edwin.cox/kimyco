import fs from 'fs';
import path from 'path';
import swaggerJsdoc from "swagger-jsdoc";

const options = require('./options.json');

const specs = swaggerJsdoc(options);

fs.writeFile(path.resolve(__dirname,'./swagger.json'), JSON.stringify(specs, null, 4), function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
}); 
